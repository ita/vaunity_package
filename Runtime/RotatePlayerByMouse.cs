﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace VAUnity
{
    public class RotatePlayerByMouse : MonoBehaviour
    {
        [SerializeField] private float speedH = 1;
        [SerializeField] private float speedV = 1;

        private float _yaw;
        private float _pitch;


        void Update()
        {
            _yaw += speedH * Input.GetAxis("Mouse X");
            _pitch += speedV * Input.GetAxis("Mouse Y");
            transform.eulerAngles = new Vector3(_pitch, _yaw, 0);
        }
    }
}