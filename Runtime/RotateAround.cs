﻿using UnityEngine;

namespace VAUnity
{

    public class RotateAround : MonoBehaviour
    {
        [SerializeField] private float radius = 5;
        [SerializeField] private float speed = 1;
        [SerializeField] private float hight = 1;
        
        
        private float timeCounter = 0;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            timeCounter += Time.deltaTime;

            var x = Mathf.Cos(speed * timeCounter) * radius;
            var z = Mathf.Sin(speed * timeCounter) * radius;

            transform.position = new Vector3(x, hight, z);

        }
    }
}
