﻿using UnityEngine;

namespace VAUnity
{
    public class MovePlayer : MonoBehaviour
    {
        [SerializeField] private float speed = 1;

        private void Update()
        {
            var moveVertical = Input.GetAxis("Vertical");
            if (moveVertical != 0)
            {
                var t = transform;
                t.position += t.forward * Time.deltaTime * speed * moveVertical;
            }
            var moveHorizontal = Input.GetAxis("Horizontal");
            if (moveHorizontal != 0)
            {
                var t = transform;
                t.position += t.right * Time.deltaTime * speed * moveHorizontal;
            }
        }
    }
}